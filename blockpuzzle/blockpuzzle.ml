(* A solver for a certain block puzzle.
 * https://www.reddit.com/r/algorithms/comments/33h1po/help_me_solve_a_block_puzzle/
 * A *piece* is a piece of wood made of two *blocks* of different *colors*,
 * on one side of that piece the corresponding faces of the blocks
 * are *slanted* in one of 4 directions, or just *flat*.
 * There are 16 pieces, the goal is to combine them
 * in two interlocked levels respecting a checkerboard pattern. *)

(* Printing format:
  * print_pieces:
    * {PIECE_ID} - dup:{NB_IDENTICAL_PIECES} - {CONSTRAINT} - {BLOCKS}
    * where:
      * PIECE_ID - Number identifying the piece shape.
      * .. - Self-descriptive.
      * CONSTRAINT - {0, 1} if constrained to level {0, 1}, 9 if no constraint.
      * BLOCKS - Slants of the two blocks, white block left, black block right.
  * pretty_print: (board/levels)
    * Each cell is made of one hex number [0-F] and one bit
    * indicating the PIECE_ID above and the color of the occupying block. *)

(* Generic helper functions. *)
module Helper = struct
  let twice f (x,y) = (f x, f y)
  let id x = x
  let on_snd f (x, y) = (x, f y)
  let swap (a, b) = (b, a)
  let index cols i j = i * cols + j
  
  (* reapply 3 f x = f (f (f x)) *)
  let rec reapply n f x = if n = 0 then x else reapply (n-1) f (f x)
  
  (* group [a;a;b;c;c;c] = [(2,a); (1,b); (3,c)] *)
  let group xs =
    let rec group = function
      | (n, x), y :: xs when x = y -> group ((n+1, x), xs)
      | x', y :: xs -> x' :: group ((1, y), xs)
      | x', [] -> [x']
    in match xs with
    | [] -> []
    | x :: xs -> group ((1, x), xs)
  
  (* choice [a; b; c] = [(a, [b; c]); (b, [a; c]); (c, [a; b])] *)
  let choice xs =
    let rec choice x1 = function
      | [] -> []
      | (1, x) as x' :: x2 -> (x, x1 @ x2) :: choice (x' :: x1) x2
      | (n, x) as x' :: x2 ->
          (x, if n == 1 then x1 @ x2 else (n-1, x) :: x1 @ x2)
            :: choice (x' :: x1) x2
    in choice [] xs
end

open Helper

(* Puzzle datatypes *)

type slant = Up | Down | Left | Right | Flat | Joker

(* The search algorithm progressively fills the "board" using this datatype. *)
type cell =
  | Free
  | Out (* Sentinel value for borders. *)
  | Filled of slant * ( int (* id of the occupying piece *)
                      * int (* color *)
                      * slant (* block direction *) )

(* By default, a piece (two blocks) is represented by a pair (slant * slant),
 * corresponding to a piece placed horizontally, slanted part up,
 * white block (color 0) on the left, black/brown block (color 1) on the right.
 * *)
type piece = slant * slant

module Print = struct
  let slant_char = function
    | Up -> '^'
    | Down -> 'v'
    | Left -> '<'
    | Right -> '>'
    | Flat -> 'o'
    | Joker -> '*'
  
  let piece_string (a, b) =
    Bytes.init 2 (function 0 -> slant_char a | _ -> slant_char b)
  
  let print_pieces =
    List.iter (fun (n, (i, (c, p))) ->
      Printf.printf "%X - dup:%d - %s - %s\n" i n (match c with Some i ->
        string_of_int i | None -> "9") (piece_string p))

  (* Print a board. *)
  let pretty_print rows cols s =
    Printf.printf "Levels 0, 1:\n";
    for i = 1 to rows do
      for k = 0 to 1 do
        for j = 1 to cols do
          match s.(2 * ((cols+2) * i + j) + k) with
          | Out -> Printf.printf "## "
          | Free -> Printf.printf ".. "
          | Filled(_, (id, c, s)) ->
              Printf.printf "%X%c "
                id
                (* (match c with 0 -> '+' | _ -> '-') *)
                (slant_char s)
        done;
        Printf.printf "   ";
      done;
      Printf.printf "\n";
    done
end

open Print

(* Helper rotation functions. *)

let flip_slant = function
  | Left -> Right
  | Right -> Left
  | d -> d

let rotate_slant = function
  | Up -> Right
  | Left -> Up
  | Down -> Left
  | Right -> Down
  | Flat -> Flat
  | Joker -> Joker

(* Turn a piece around, so that the slants face down,
 * leaving the left block on the left, the right block on the right. *)
let flip_piece = twice flip_slant

(* Quarter turn on each component. Note that the orientation of the piece
 * as a whole is not explicit. *)
let quarter_turn_piece = twice rotate_slant

(* Anti-clockwise *)
let acw_quarter_turn_piece = reapply 3 quarter_turn_piece

(* 2 quarter turns and swap. Useful to exchange black and white. *)
let half_turn_piece x = x |> reapply 2 quarter_turn_piece |> swap

(* Checks that a newly placed piece faces either an empty cell or
 * one with the same slant. *)
let compatible slant cell =
  match slant, cell with
  | _, Free -> true
  | _, Out -> false
  | s, Filled(s', _) -> s = s' || s = Joker || s' = Joker

(* Represent the two levels in one single array,
 * initialized with sentinels on the borders.
 * 
 * ######
 * #....# 0
 * #....# 1
 * #....# 2
 * #....# 3
 * ######
 *
 * the array above, linearized, twice.
 *
 * The cell in the i-th row and j-th column of level k (0 or 1)
 * has index ((m+2) * (i+1) + j+1) * 2 + k
 *
 * Could be made more compact but it shouldn't matter much for complexity.
 * *)

type space = int * int * cell array

(* Fresh empty board. *)
let puzzle_space rows cols =
  let s = Array.make ((cols + 2) * (rows + 2) * 2) Free in
  let set_out x =
    s.(2*x) <- Out;
    s.(2*x+1) <- Out;
  in
  for j = 0 to cols+1 do
    set_out j;
    set_out ((cols+2) * (rows+1) + j);
  done;
  for i = 1 to rows do
    set_out ((cols+2)*i);
    set_out ((cols+2)*(i+1)-1);
  done;
  s

(* Gather pieces *)
let group_pieces ps =
  ps |> List.sort compare |> group |> List.mapi (fun i (n, p) -> n, (i, p))

(* Scan available positions by rows first, by columns second, by levels third;
 * making use of local constraints early on.
 * Flip the pieces if in level 1.
 * For each piece try two orientations (two others are impossible due to the
 * scanning method). *)
exception Found
type tpieces =
  ( int (* piece repeat *)
  * ( int (* id *) 
    * (int option (* level constraint *) * piece))) list

(* The Found exception is caught inside this function, and a filled board
 * is returned instead.
 * Raises Not_found if no solution exists. *)
let solve rows cols pieces =
  let s = puzzle_space rows cols in
  let color x =
    let y = x / 2 in
    let i = y / (cols + 2) in
    let j = y mod (cols + 2) in
    (i + j) mod 2
  in
  let rec search x ps =
    match s.(x) with
    | _ when x = 2 * (rows+1) * (cols+2) -> raise Found
    | Out | Filled(_, _) -> search (x+1) ps
    | Free ->
        ps |> choice |> List.iter (fun (p, ps) ->
          let c = color x in
          let pid, q = p in
          let l, p' = q in
          let search_at y a b =
            if s.(y) = Free
              && compatible a s.(x lxor 1)
              && compatible b s.(y lxor 1)
            then begin
              s.(x) <- Filled(a, (pid, c, a));
              s.(y) <- Filled(b, (pid, 1-c, b));
              search (x+1) ps;
              s.(y) <- Free;
            end
          in
          if Some ((1 + x) mod 2) <> l then begin
            (* Try fitting the piece horizontally. *)
            let a, b as p' =
              p' |> (if x mod 2 = 0 then id else flip_piece)
                |> (if c = 0 then id else half_turn_piece) in
            let y = x + 2 in
            search_at y a b;
            (* Retry vertically. *)
            let a, b = quarter_turn_piece p' in
            let y = x + 2*(cols+2) in
            search_at y a b;
          end);
        s.(x) <- Free
  in try search 0 pieces; raise Not_found with Found -> s

(* Generate random solvable puzzles. *)
module RandomPuzzles = struct
  (* Generate a random partition of a rows * cols matrix into dominoes. *)
  let dominoes rows cols =
    let index = index cols in
    let free = Array.make (rows * cols) true in
    let ret = ref [] in
    let return ps = ret := ps; raise Found in
    let rec backtrack ps x =
      if x = rows * cols then return ps;
      let i, j = x / cols, x mod cols in
      if free.(x) then begin
        free.(x) <- false;
        let dir = Random.bool () in
        List.iter (fun dir ->
            let i', j' = if dir then i, j+1 else i+1, j in
            let y = index i' j' in
            if i' < rows && j' < cols && free.(y) then begin
              free.(y) <- false;
              backtrack ((dir, (x, y)) :: ps) (x+1);
              free.(y) <- true;
            end
          ) [dir ; not dir];
        free.(x) <- true;
      end else backtrack ps (x+1)
    in try backtrack [] 0; raise Not_found with Found -> !ret
  
  (* Generate a random puzzle with a couple of flat pieces. *)
  let generate rows cols =
    let index = index cols in
    (* Two levels of imbrication. *)
    let pieces = Array.init 2 (fun _ -> dominoes rows cols) in
    (* The initial array contains no Flat cell. *)
    let s = Array.init (rows * cols) (fun _ ->
      match Random.int 4 with
      | 0 -> Up
      | 1 -> Down
      | 2 -> Left
      | _ -> Right
    ) in
    (* Flatten two adjacent cells *)
    let dir = Random.bool () in
    let i, j =
      let open Random in
      if dir then int rows, int (cols - 1) else int (rows - 1), int cols
    in
    let i', j' = if dir then i, j+1 else i+1, j in
    s.(index i j) <- Flat;
    s.(index i' j') <- Flat;
    pieces
      |> Array.map (List.map (fun (horiz, (a, _ as p)) ->
        let turns, swap' =
          match horiz, (a / cols + a mod cols) mod 2 with
          | true, 0 -> 0, id
          | true, 1 -> 2, swap
          | false, 0 -> 3, id
          | false, 1 -> 1, swap
          | _ -> assert false
        in
        p |> twice (Array.get s) |> reapply turns quarter_turn_piece |> swap'))
      |> Array.mapi (fun i ps ->
          List.map (fun (a, b as p) ->
              (if a = Flat || b = Flat then Some i else None), p
            ) (if i = 0 then ps else List.map flip_piece ps))
      |> Array.to_list |> List.flatten |> group_pieces
end

open RandomPuzzles

(* Hard-coded set of pieces. *)
let rows = 4
let cols = 4

let pieces =
  [ None, (Up, Up)
  ; None, (Up, Up)
  ; None, (Up, Down)
  ; None, (Up, Left)
  ; None, (Down, Up)
  ; None, (Down, Right)
  ; None, (Down, Right)
  ; None, (Left, Up)
  ; None, (Left, Right)
  ; None, (Left, Right)
  ; None, (Right, Up)
  ; None, (Right, Left)
  ; None, (Right, Left)
  ; Some 1, (Down, Flat)
  ; Some 1, (Flat, Left)
  ; Some 0, (Flat, Flat)
  ] |> List.map (on_snd (acw_quarter_turn_piece)) 
    |> group_pieces

let () = Random.self_init ()

module Parser = struct
  let slant_assoc =
    [ '^', Up
    ; 'v', Down
    ; '<', Left
    ; '>', Right
    ; 'o', Flat
    ; '*', Joker
    ]

  let rec parse h =
    try
      let open String in
      let s = trim (input_line h) in
      let fail () =
        failwith "Invalid input. Expected: [^v<>o]{2}[01]?"
      in
      if length s = 0 then
        parse h
      else if length s < 2 then
        fail ()
      else
        let c1 = List.assoc s.[0] slant_assoc in
        let c2 = List.assoc s.[1] slant_assoc in
        let opt =
          if length s > 2 then
            Some (match s.[2] with '0' -> 0 | '1' -> 1 | _ -> fail ())
          else
            None
        in (opt, (c1, c2)) :: parse h
    with End_of_file -> []

  let parse_pieces h =
    Scanf.sscanf (input_line h) "%d %d" (fun rows cols -> rows, cols, parse h)
end
      
open Parser

type mode = RAND | SOLVE | THAT

(* Arguments:
  * none or "solve": solve the hard coded problem.
  * "rand": read three integers, [r c n], from the input, and
  * solve n instances of size r * c. *)
let () =
  let mode =
    let open Array in
    if length Sys.argv = 1 then
      SOLVE
    else match Sys.argv.(1) with
    | "rand" -> RAND
    | "that" -> THAT
    | "solve" -> SOLVE
    | _ -> failwith "Invalid argument (rand|that|(solve)?)"
  in
  let solve' r c ps v =
    if v then print_pieces ps;
    let s = solve r c ps in (* Raises Not_found if unsolvable. *)
    if v then pretty_print r c s
  in
  match mode with
  | RAND ->
      Scanf.scanf "%d %d %d" (fun rows cols n ->
        for i = 0 to n-1 do
          let inst = generate rows cols in
          solve' rows cols inst (i mod 499 = 0)
        done)
  | SOLVE ->
      let rows, cols, ps = parse_pieces stdin in
      let inst = ps |> group_pieces in
      solve' rows cols inst true
  | THAT -> solve' rows cols pieces true

