let n = 10
let m = 10
let ships = [5;4;3;3;2]

let a = Array.make_matrix n m false

let count = ref 0

exception Break

let check h i j s =
  try
    if h = 0 then (
      if i+s > n then raise Break;
      for k = i to i+s-1 do
        if a.(k).(j) then raise Break;
      done
    ) else (
      if j+s > m then raise Break;
      for k = j to j+s-1 do
        if a.(i).(k) then raise Break;
      done
    );
    true
  with
  | Break -> false

let set h i j s x =
  if h = 0 then
    for k = i to i+s-1 do
      a.(k).(j) <- x
    done
  else
    for k = j to j+s-1 do
      a.(i).(k) <- x
    done

let rec f s ships h i j =
  if h = 2 then f s ships 0 (i+1) j
  else if i = n then f s ships 0 0 (j+1)
  else if j = m then ()
  else (
    if check h i j s then (
      match ships with
      | [] -> incr count
      | s' :: ships ->
          set h i j s true;
          if s = s' then
            f s' ships 0 (i+1) j
          else
            f s' ships 0 0 0;
          set h i j s false
    );
    f s ships (h+1) i j
  )

let _ =
  match ships with
  | [] | _ :: [] -> assert false
  | s :: s' :: ships ->
      assert (n mod 2 <> s mod 2);
      assert (m mod 2 = 0);
      for i = 0 to (n-s) / 2 do
        for j = 0 to m / 2 - 1 do
          set 0 i j s true;
          f s' ships 0 0 0;
          set 0 i j s false
        done
      done;
      print_int (8 * !count);
      print_newline ()
