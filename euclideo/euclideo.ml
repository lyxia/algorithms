(* Compass and straight-edge solver. *)

type length = float

module Float = struct
  let precision = 1e-10
  let (=~) a b = abs_float (a -. b) < precision
  let normalize a = floor (a /. precision +. 0.5)
end

module Point = struct
  type t = length * length
  let (=~) (x1, y1) (x2, y2) = Float.(x1 =~ x2 && y1 =~ y2)
  let mem p0 = List.exists ((=~) p0)
  let compare (x1, y1) (x2, y2) =
    if Float.(x1 =~ x2) then
      compare y1 y2
    else
      compare x1 x2
  let normalize (x, y) = Float.(normalize x, normalize y)
end

module ShapeType = struct
  (* A canonical representation of straight lines and circles. *)
  type shape

    = Circle of Point.t * length
    (* Circle (center, radius ** 2) *)

    | Line of length * length * length
    (* Line (a, b, c)
     * |(a, b)| = 1.
     * a >= 0, b > -1.
     * ax + by = c *)

    | Segment of (length * length * length) * Point.t * Point.t
end

include ShapeType

module Shape = struct
  type t = shape

  let (=~) s1 s2 =
    match s1, s2 with
    | Line (a, b, c), Line (a', b', c') -> Float.(a =~ a' && b =~ b' && c =~ c')
    | Circle (p, rr), Circle (p', rr') -> Point.(p =~ p') && Float.(rr =~ rr')
    | _ -> false

  let mem s0 = List.exists ((=~) s0)

  let compare s1 s2 =
    match s1, s2 with
    | Line (a, b, c), Line (a', b', c') ->
        if Float.(a =~ a') then
          if Float.(b =~ b') then
            compare c c'
          else
            compare b b'
        else
          compare a a'
    | Circle (p, rr), Circle (p', rr') ->
        if Point.(p =~ p') then
          compare rr rr'
        else
          Point.compare p p'
    | Line _, Circle _ -> -1
    | Circle _, Line _ -> 1
    | Segment _, _ -> -1
    | _, Segment _ -> 1

  let normalize = function
    | Circle (p, rr) -> Circle (Point.normalize p, Float.normalize rr)
    | Line (a, b, c) -> Float.(Line (normalize a, normalize b, normalize c))
    | Segment ((a, b, c), p1, p2) -> Segment (Float.(normalize a, normalize b, normalize c), Point.normalize p1, Point.normalize p2)
end

let intersect_line_line (a1, b1, c1) (a2, b2, c2) =
  let det = a1 *. b2 -. b1 *. a2 in
  (* Parallel lines are equal or have 0 intersection. *)
  if Float.(det =~ 0.) then
    []
  else
    let p = (b2 *. c1 -. b1 *. c2) /. det, (a1 *. c2 -. a2 *. c1) /. det in
    [p]

let intersect_line_circle (a, b, c) (x, y) rr =
  let c = c -. a *. x -. b *. y in
  let d1 = a *. a +. b *. b in
  let delta = rr *. d1 -. c *. c in
  let delta = if Float.(delta =~ 0.) then 0. else delta in
  if delta < 0. then
    []
  else
    let d' = sqrt delta in
    let xa = (a *. c +. b *. d') /. d1 +. x in
    let ya = (b *. c -. a *. d') /. d1 +. y in
    let xb = (a *. c -. b *. d') /. d1 +. x in
    let yb = (b *. c +. a *. d') /. d1 +. y in
    [(xa, ya); (xb, yb)]

let intersect_circle_circle ((x1, y1) as p1) rr1 (x2, y2) rr2 =
  (* Concentric circles are equal or have 0 intersection. *)
  if Float.(x1 =~ x2 && y1 =~ y2) then
    []
  else
    let a = 2. *. (x2 -. x1) and b = 2. *. (y2 -. y1) in
    let c = rr1 -. x1 *. x1 -. y1 *. y1 -. rr2 +. x2 *. x2 +. y2 *. y2 in
    intersect_line_circle (a, b, c) p1 rr1

(* 0, 1 or 2 intersection points. Equal shapes are ignored. *)
let rec intersect : Shape.t -> Shape.t -> Point.t list = fun s1 s2 ->
  match s1, s2 with
  | Line (a1, b1, c1), Line (a2, b2, c2) -> intersect_line_line (a1, b1, c1) (a2, b2, c2)
  | Line (a, b, c), Circle (p, rr)
  | Circle (p, rr), Line (a, b, c) -> intersect_line_circle (a, b, c) p rr
  | Circle (p1, rr1), Circle (p2, rr2) -> intersect_circle_circle p1 rr1 p2 rr2
  | Segment (abc, p1, p2), w | w, Segment (abc, p1, p2) -> intersect_segment (abc, p1, p2) w
and intersect_segment ((a, b, c), ((x1, y1) as p1), ((x2, y2) as p2)) w =
  let dx = x2 -. x1 and dy = y2 -. y1 in
  let norm = dx *. dx +. dy *. dy in 
  let between ((x, y) as p) =
    Point.(p =~ p1 || p =~ p2)
    || ((x -. x1) *. dx +. (y -. y1) *. dy) /. (norm *. norm) <= 1. in
  List.filter between (intersect (Line (a, b, c)) w)

let make_circle (x1, y1) (x2, y2) =
  let dx = x1 -. x2 and dy = y1 -. y2 in
  Circle ((x1, y1), dx *. dx +. dy *. dy)

let make_line (x1, y1) (x2, y2) =
  let a' = y2 -. y1 in
  let b' = x1 -. x2 in
  let d' = sqrt (a' *. a' +. b' *. b') in
  let a = a' /. d' and b = b' /. d' in
  let a, b =
    if Float.(b =~ -1. || b =~ 1.) then
      0., 1.
    else if a >= 0. then
      a, b
    else
      -.a, -.b in
  Line (a, b, a *. x1 +. b *. y1)

type configuration = {
  points : Point.t list ;
  shapes : (Shape.t * Graphics.color) list ;
}

let rec print_shape h = function
  | Circle ((x, y), rr) -> Printf.fprintf h "(%.2f, %.2f) %.2f" x y (sqrt rr)
  | Line (a, b, c) -> Printf.fprintf h "%.2fx + %.2fy = %.2f" a b c
  | Segment ((a, b, c), _, _) -> print_shape h (Line (a, b, c))

let print_shapes h = Printf.fprintf h "[%a]"
  (fun h -> List.iter (Printf.fprintf h "%a; " (fun h (s, _) -> print_shape h s)))

let print_points h = Printf.fprintf h "[%a]"
  (fun h -> List.iter (fun (x, y) -> Printf.fprintf h "%.2f %.2f; " x y))

let print_conf conf =
  Printf.printf "{ shapes = %a ;\n  points = %a ;\n}\n%!"
    print_shapes conf.shapes print_points conf.points

let serialize_shape h = function
  | Circle ((x, y), rr) -> Printf.fprintf h "C %f %f %f\n" x y (sqrt rr)
  | Line (a, b, c) -> Printf.fprintf h "L %f %f %f\n" a b c
  | Segment ((a, b, c), (x1, y1), (x2, y2)) -> Printf.fprintf h "S %f %f %f %f %f %f %f\n" a b c x1 y1 x2 y2

let serialize_shapes h =
  List.iter (fun (s, _) -> serialize_shape h s)

let serialize_conf h conf =
  Printf.fprintf h "%d\n%a%!"
    (List.length (conf.shapes))
    serialize_shapes conf.shapes

let rec tiered_pairs_stream ~new_points ~old_points =
  match new_points with
  | [] -> []
  | p :: new_points ->
    let rec points_stream points tail =
      match points with
      | [] -> tail
      | p' :: points ->
          (p, p') :: points_stream points tail in
    points_stream new_points
      (points_stream old_points
        (tiered_pairs_stream new_points old_points))

let member shape (x, y) =
  match shape with
  | Line (a, b, c) -> Float.(a *. x +. b *. y =~ c)
  | Circle ((x', y'), rr) ->
      let dx = x -. x' in
      let dy = y -. y' in
      Float.(dx *. dx +. dy *. dy =~ rr)
  | Segment _ -> assert false

let new_shapes_stream new_points old_points =
  let make_shapes (p1, p2) =
    let line = make_line p1 p2 in
    let circle1 = make_circle p1 p2 in
    let circle2 = make_circle p2 p1 in
    let (|?) a b = if b then [] else a in
    ([line] |? (List.length (List.filter (member line) old_points) >= 2))
    @ [circle1]
    @ ([circle2] |? (Point.mem p2 old_points && List.exists (member circle2) old_points)) in
  List.flatten
    (List.map
      make_shapes
      (tiered_pairs_stream ~new_points ~old_points))

let new_shape_intersects conf s =
  let old_points = conf.points in
  List.fold_left
    (fun new_points (s', _) ->
      List.fold_left
        (fun new_points p ->
          if Point.mem p new_points || Point.mem p old_points then
            new_points
          else
            p :: new_points)
        new_points
        (intersect s s'))
    []
    conf.shapes

let normalize_conf conf =
  List.sort compare (List.map (fun (s, _) -> Shape.normalize s) conf.shapes)

exception Skip

(* Apply the continuation on configurations which contain
 * any of the target points or shapes. *)
let search ~target_shapes ~target_points ~max_new_shapes conf k =
  let memo = Hashtbl.create 1000 in
  let memo_final = Hashtbl.create 1000 in
  let rec search max_new_shapes new_points conf stream =
    let add_shape new_points s stream =
      let fresh_points = new_shape_intersects conf s in
      let new_conf = { conf with shapes = (s, Graphics.black) :: conf.shapes } in
      if max_new_shapes > 3 then (
        let conf_ = normalize_conf new_conf in
        if Hashtbl.mem memo conf_ then raise Skip;
        Hashtbl.add memo conf_ ());
      if Shape.mem s target_shapes
        || List.exists (fun p -> Point.mem p target_points) fresh_points
      then (
        let conf_ = normalize_conf new_conf in
        if Hashtbl.mem memo_final conf_ then raise Skip;
        Hashtbl.add memo_final conf_ ();
        k new_conf)
      else
        search
          (max_new_shapes - 1)
          (fresh_points @ new_points)
          new_conf
          stream in
    let add_shape new_points s stream =
      try
        add_shape new_points s stream
      with
      | Skip -> () in
    if max_new_shapes > 0 then (
      match stream with
      | [] ->
          ( match new_points with
          | [] -> ()
          | _ ->
              search
                max_new_shapes
                []
                { conf with points = new_points @ conf.points }
                (new_shapes_stream new_points conf.points)
          )
      | s :: stream ->
          add_shape new_points s stream;
          search max_new_shapes new_points conf stream
    ) in
  search max_new_shapes conf.points { conf with points = [] } []

let draw_conf title conf =
  let open Graphics in
  open_graph "";
  set_window_title title;
  let sz_x = ref 800 in
  let sz_y = ref 600 in
  let o_x = ref 400 in
  let o_y = ref 300 in
  let i = ref 100. in (* 1 geometric unit in pixels. *)
  let scale d = int_of_float (d *. !i) in
  let scale_x x = scale x + !o_x in
  let scale_y y = scale y + !o_y in
  let unscale d = float_of_int d /. !i in
  let unscale_x x = unscale (x - !o_x) in
  let unscale_y y = unscale (y - !o_y) in
  let draw_shape (s, c) =
    set_color c;
    match s with
    | Circle ((x, y), rr) ->
        draw_circle (scale_x x) (scale_y y) (scale (sqrt rr))
    | Line _ as line ->
        let x_min = unscale_x 0 in
        let y_min = unscale_y 0 in
        let x_max = unscale_x !sz_x in
        let y_max = unscale_y !sz_y in
        let center = (x_min +. x_max) /. 2., (y_min +. y_max) /. 2. in
        let radius = max (x_max -. x_min) (y_max -. y_min) *. 10. in
        ( match intersect line (Circle (center, radius *. radius)) with
        | [(x1, y1); (x2, y2)] ->
            moveto (scale_x x1) (scale_y y1);
            lineto (scale_x x2) (scale_y y2)
        | _ -> ()
        )
    | Segment (_, (x1, x2), (y1, y2)) ->
        moveto (scale_x x1) (scale_y y1);
        lineto (scale_x x2) (scale_y y2) in
  resize_window !sz_x !sz_y;
  List.iter draw_shape conf.shapes;
  set_color Graphics.magenta;
  fill_circle !o_x !o_y 2;
  let continue = ref true in
  while !continue do
    continue := (read_key () <> 'q')
  done;
  close_graph ()

let draw_solutions ~target_shapes ~target_points ~max_new_shapes conf =
  let counter = ref 0 in
  search ~target_shapes ~target_points ~max_new_shapes conf
    (fun conf ->
      incr counter;
      let title = "Solution " ^ string_of_int !counter in
      print_endline title;
      print_conf conf;
      draw_conf title conf)
