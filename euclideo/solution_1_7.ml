open Euclideo

let go_1_7 () =
  draw_solutions
    ~target_shapes:[make_line (1., 0.) (0., -1.)]
    ~target_points:[]
    ~max_new_shapes:4
    { shapes = [
        Circle ((0., 0.), 1.), Graphics.blue];
      points = [
        0., 0.;
        0., 1.]
    }

let () = go_1_7 ()
