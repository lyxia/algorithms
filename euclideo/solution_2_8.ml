open Euclideo

let go_2_8 () =
  Random.self_init ();
  let random_point () =
    let r = Random.float 3.14 in
    cos r, sin r in
  draw_solutions
    ~target_shapes:[make_line (1., 0.) (1., 1.)]
    ~target_points:[]
    ~max_new_shapes:3
    { shapes = [
        Circle ((0., 0.), 1.), Graphics.blue];
      points = [
        0., 0.;
        1., 0.;
        random_point ()]
    }

let () = go_2_8 ()
