open Euclideo

let go_3_8 () =
  let pi_4 = atan 1. in
  let x4 = cos pi_4 in
  search
    ~target_shapes:[make_line (x4, x4) (x4 +. 1., x4)]
    ~target_points:[]
    ~max_new_shapes:7
    { shapes = [
        Segment ((0., 1., 0.), (0., 0.), (1., 0.)), Graphics.blue];
      points = [
        0., 0.;
        1., 0.;
        ]
    }
    (serialize_conf stdout)

let () = go_3_8 ()
