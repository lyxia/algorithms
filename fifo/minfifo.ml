(* "Classic" problem
   Queue-like data structures that support efficient operations:
   - get_min
   - push_back
   - pop_front
   and optionally,
   - pop_min
   - pop_back
   Different variants are to be discussed.
 *)


(* Signature of an ordering on some type. *)
module type Ord = sig type t val compare : t -> t -> int end

(* Default ordering on int. *)
module OrdInt : Ord with type t = int = struct
  type t = int
  let compare = compare
end

(* Some exceptions. *)
let empty_queue = Invalid_argument "Empty queue."
let full_queue = Invalid_argument "Full queue."


(************************* 
   A mutable structure for
   - get_min,   O(1);
   - push_back, O(1) amortized, worst case O(log(size of queue)) with binary
     search;
   - pop_front, O(1).
   We assume that the value popped by pop_front does not matter,
   as opposed to other examples below.
   
   For example, such a situation arises if we just want to keep
   track of the minimum of the latest n values in a stream.
   This allows some values to be discarded before a request to pop them
   even comes.

 *************************)
module Queue1 (Ord : Ord) = struct
  (* The size of int puts a bound on the difference between
     the number of pops and of pushes:
     although pushed and popped can be allowed to loop back,
     pushed should not catch up to popped from below.

     Invariant: For all i s.t. front <= i < back - 1, buffer.(i) < buffer.(i+1)
     where (x,m) < (y,n) iff x < y and m < n (up to some details due to memory
     boundedness.)

     The second projection of the pair is the "insertion index", i.e.,
     the rank of the element in the order of insertion.
   *)
  type t = {
    mutable buffer : (Ord.t * int) array ; (* Dynamic array *)
    mutable front  : int ;
    mutable back   : int ;
    mutable pushed : int ;
    mutable popped : int }

  (*** new_queue : Ord.t -> t ***)
  (* Only nonempty queues can be created but that's mostly an artifact
     due to the language's not allowing uninitialized array *)
  let new_queue x = 
    let start_size = 16
    in {
      buffer = Array.make start_size (x, 0) ;
      front  = 0 ;
      back   = 1 ;
      pushed = 1 ;
      popped = 0 }

  let length q = Array.length q.buffer

  (* Dynamic array grow operation. Assumes that the buffer is non-empty. *)
  let grow q =
    let n = length q in
    let buffer = Array.make (2 * n) q.buffer.(0) in
    if q.back < q.front then begin
      Array.blit q.buffer 0 buffer 0 q.back;
      Array.blit q.buffer q.front buffer (n + q.front) (n - q.front);
      q.front <- q.front + n;
    end else begin
      Array.blit q.buffer q.front buffer q.front (q.back - q.front);
    end;
    q.buffer <- buffer

  let is_empty q = q.front = q.back

  let is_full q = q.pushed + 1 = q.popped

  let assert_nonempty q = if is_empty q then raise empty_queue

  let assert_notfull q = if is_full q then raise full_queue

  (*** get_min : t -> Ord.t ***)
  (* Due to the invariant, the minimum is simply the front element. *)
  let get_min q =
    assert_nonempty q;
    fst q.buffer.(q.front)

  (*** pop_front : t -> unit ***)
  (* Due to the invariant, the oldest of the physically stored elements
     is also the front element, and popping the oldest virtual element is
     just a matter of comparing their insertion indices.
   *)
  let pop_front q =
    assert_nonempty q;
    if snd q.buffer.(q.front) = q.popped then
      q.front <- (q.front + 1) mod length q;
    q.popped <- q.popped + 1

  (*** push_back : Ord.t -> t -> unit ***)
  (* A new element is inserted from the back, so that the invariant is
     preserved.
   *)
  let push_back x q =
    assert_notfull q;
    (* Find insertion point by binary search. *)
    let rec bsearch i j =
      if i = j then
        i
      else begin
        let mid = (i + j)/2 in
        match Ord.compare (fst q.buffer.(mid mod length q)) x with
        | x when x < 0 -> bsearch (mid + 1) j
        | _ -> bsearch i mid
      end
    in
    let i =
      if q.front <= q.back then
        bsearch q.front q.back
      else
        bsearch q.front (q.back + length q)
    in
    if (i + 1) mod length q = q.front then grow q;
    q.buffer.(i) <- (x, q.pushed);
    q.pushed <- q.pushed + 1;
    q.back <- (i + 1) mod length q
end


(****************************
   An immutable structure for
   - get_min,   O(1);
   - push_back, O(1);
   - pop_front, O(1) amortized, worst case O(size of queue).
   
 ****************************)
module Queue2 (Ord : Ord) = struct
  (* We choose to implement this queue with a pair of lists.
     It should also be possible with circular buffers as above,
     at the cost of mutability.
     For each of the record members, the second projection of the head
     is the smallest element of the whole list. *)
  type t = {
    in_stack : (Ord.t * Ord.t) list ;
    out_stack : (Ord.t * Ord.t) list }

  (*** empty : t ***)
  let empty = { in_stack = [] ; out_stack = [] }

  let is_empty q = q = empty

  (* This structure relies mainly on the ability to find the minimum
     of two values. *)
  let min x y = if Ord.compare x y < 0 then x else y

  (*** get_min : t -> Ord.t ***)
  let get_min q =
    match q with
    | { in_stack = [] ; out_stack = (_, m) :: _ }
    | { in_stack = (_, m) :: _ ; out_stack = [] }
      -> m
    | { in_stack = (_, m) :: _ ; out_stack = (_, m') :: _ }
      -> min m m'
    | _ -> raise empty_queue

  (*** push_back : Ord.t -> t -> t ***)
  let push_back x q = match q with
    | { in_stack = [] }
      -> { q with in_stack = [(x, x)] }
    | { in_stack = ((_, m) :: _) as is }
      -> { q with in_stack = (x, min x m) :: is}

  (*** pop_front : t -> Ord.t * t ***)
  let pop_front q =
    let rec reverse' l aux = match l, aux with
      | [], _ -> aux
      | (x, _) :: t, [] -> reverse' t [(x, x)]
      | (x, _) :: t, (_, y) :: _ -> reverse' t ((x, min x y) :: aux)
    in match q with
    | { out_stack = (x, _) :: t }
       -> (x, { q with out_stack = t })
    | { out_stack = [] ; in_stack = is }
      -> match reverse' is [] with
         | (x, _) :: t -> (x, { in_stack = [] ; out_stack = t })
         | [] -> raise empty_queue
end


(************************************************ 
   A Cartesian tree based structure for
   - get_min, O(1);
   - push_back, O(1) amortized, worst case O(size of queue);
   - pop_front, O(1) amortized, worst case O(size of queue).

 ************************************************)
module Queue3 (Ord : Ord) = struct
  (* A Cartesian tree is a binary tree such that
     it has the heap property (parent <= child) and
     an in-order traversal visits the nodes in their insertion order. *)
  (* Pretty much a node option. *)
  type ctree = Leaf | Node of node

  and node = {
            value : Ord.t ;
    mutable up : ctree ;
    mutable left : ctree ;
    mutable right : ctree }

  (* We use a pointer to the earliest, latest, and smallest element. *)
  type t = {
    mutable root : ctree ;
    mutable front : ctree ;
    mutable back : ctree }

  let is_empty q = q.root = Leaf

  let mkNode v u l r = { value = v ; up = u ; left = l ;  right = r }

  (*** new_queue : unit -> t ***)
  let new_queue () = { root = Leaf ; front = Leaf ; back = Leaf }

  (*** get_min : t -> Ord.t ***)
  (* Thanks to the heap property, the smallest element is precisely the root. *)
  let get_min q = match q.root with
    | Node { value = m } -> m
    | Leaf -> raise empty_queue

  (*** push_back : Ord.t -> t -> unit ***)
  (* The latest element must be the rightmost node, i.e.,
     the right child of the right child of ... of the root.
     We start looking for the insertion point from the rightmost node
     and up the spine. *)
  let push_back x q =
    let new_n = mkNode x Leaf Leaf Leaf in
    let newT = Node new_n in
    let rec insert = function
      | Leaf (* New root. *) ->
        begin match q.root with
          | Leaf ->
            q.front <- newT;
          | Node root ->
            root.up <- newT;
            new_n.left <- q.root;
        end;
        q.root <- newT

      | Node ({ value = y } as node) when Ord.compare y x >= 0 ->
          insert node.up

      | Node node as nodeT (* y < x *) ->
        begin match node.right with
          | Leaf -> ()
          | Node right as rightT ->
            (* The right child is moved to the left of the new node. *)
            right.up <- newT;
            new_n.left <- rightT;
        end;
        (* The new node takes the position of right child of node. *)
        node.right <- newT;
        new_n.up <- nodeT;
    in
    insert q.back;
    q.back <- newT

  (*** pop_front : t -> Ord.t ***)
  (* The earliest element must be the leftmost node.
     Some bookkeeping is necessary to ensure that after removal. *)
  let pop_front q = match q.front with
    | Leaf -> raise empty_queue
    | Node front ->
      begin match front.right, front.up (* It cannot have a left child. *) with
        | Leaf, Leaf (* Queue emptied. *) ->
          q.front <- Leaf;
          q.root <- Leaf;
          q.back <- Leaf

        | Node right as rightT, Leaf (* Front is root. *) ->
          q.front <- rightT;
          q.root <- rightT;
          right.up <- Leaf

        (* In other cases, the right child takes the freed position,
           and we find the new leftmost node. *)
        | Leaf, (Node up as upT) ->
          up.left <- Leaf;
          q.front <- upT

        | (Node right as rightT), (Node up as upT) ->
          up.left <- rightT;
          right.up <- upT;
          let rec leftmost = function
            | { left = Node l } -> leftmost l
            | { left = Leaf } as left -> Node left
          in
          q.front <- leftmost right
      end;
      front.value
end

