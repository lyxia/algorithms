# Le graphe est donne par une liste d'adjacence : graph[i] est
# la liste des voisins de i.

def max_matching_size(graph):
    n = len(graph)

    # Un couplage. Une arete (i, j) y appartient si matching[i] = j et
    # matching[j] = i. Les sommets non-couverts k ont matching[k] = -1.
    matching = [-1] * n
    matching_size = 0

    # Cherche un chemin augmentant partant de r, et effectue l'augmentation
    # associee s'il y en a un.
    def plant_tree(r):
        # L'arbre A des sommets pairs est donne par le tableau de predecesseurs.
        # pred[i] = -1 si i n'est pas dans l'arbre, ou si i == r.
        pred = [-1] * n
        # File d'aretes a examiner
        queue = [ (r, j) for j in graph[r] ]

        # Si on trouve un sommet pair i dont le voisin j est expose,
        # il y a un chemin augmentant. On augmente alors le couplage.
        # (a b-c d-e...x-y z) devient (a-b c-d e...x y-z)
        def augment(i, j):
            nonlocal matching_size
            matching_size += 1
            while i != -1:
                mi = matching[i]
                matching[i] = j
                matching[j] = i
                i, j = pred[i], mi

        # Liste des sommets d'indices impairs sur le chemin de i vers r le long de A
        def path_from(i):
            p = []
            while i != r:
                p.append(matching[i])
                i = pred[i]
            return p

        def strip_common_suffix(p, q):
            while p and q and p[-1] == q[-1]:
                p.pop()
                q.pop()

        # Il faut s'arreter au dernier sommet qui n'est pas deja dans A
        def trim_outer(p):
            while p and pred[p[-1]] != -1:
                p.pop()

        # On forme un chemin p de sommets pairs partant de i,
        # ajoutant de nouveaux sommets a A.
        def backtrace(i, p):
            nonlocal queue
            for j in p:
                if pred[j] == -1:
                    queue += [ (j, k) for k in graph[j] ]
                pred[j] = i
                i = j

        while queue:
            (i, j) = queue.pop()
            if j == r or matching[j] == i:
                continue
            if matching[j] == -1:
                augment(i, j)
                return
            elif pred[j] != -1:
                pi = path_from(i)
                pj = path_from(j)
                strip_common_suffix(pi, pj)
                trim_outer(pi)
                backtrace(j, pi)
                trim_outer(pj)
                backtrace(i, pj)
            elif pred[matching[j]] == -1:
                backtrace(i, [matching[j]])

    for r in range(n):
        if matching[r] == -1:
            plant_tree(r)
    return matching_size

def read_graph():
    n = int(input())
    m = int(input())
    graph = [[] for i in range(n)]
    for i in range(m):
        (ra, rb) = list(map(int, input().split()))
        graph[ra].append(rb)
        graph[rb].append(ra)
    return graph

graph = read_graph()
print(len(graph) - max_matching_size(graph))
