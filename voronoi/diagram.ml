(* Representing a Voronoi diagram: a graph with rays to infinity. *)

type point = float * float

type direction = float * float

type edge
  = Edge of point * point (* An edge is bounded by two points. *)
  | Ray of point * direction (* A ray is bounded by one point. *)
  | Line of point * point
  (* A line extends to infinity in both directions.
   * It is given as the bisector of two points. *)

type diagram = {
  vertices : point list ;
  edges : edge list ;
}

let fprint_edge h = function
  | Edge ((ux, uy), (vx, vy)) ->
      Printf.fprintf h "Edge (%.3f, %.3f) - (%.3f, %.3f)" ux uy vx vy
  | Ray ((ux, uy), (dx, dy)) ->
      Printf.fprintf h "Ray (%.3f, %.3f) -+ (%.3f, %.3f)" ux uy dx dy
  | Line ((ux, uy), (vx, vy)) ->
      Printf.fprintf h "Line (%.3f, %.3f) / (%.3f, %.3f)" ux uy vx vy

let print_diagram diagram =
  List.iter (fun e -> fprint_edge stdout e; Printf.printf "\n") diagram.edges

