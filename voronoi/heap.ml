(* A reference is considered null if the heap_ref field is negative. *)
type 'a r' = {
  elt : 'a ;
  mutable heap_ref : int ;
}

(* A binary min-heap that updates references to its elements. *)
module Make(Ord : sig
  type elt
  val compare : elt -> elt -> int
end) = struct
  open Array
  include Ord

  type r = Ord.elt r'

  type t = {
    mutable heap : r array;
    mutable len : int;
  }

  let create () = {
    heap = [||] ;
    len = 0 ;
  }

  let update t i = t.heap.(i).heap_ref <- i

  let swap t i j =
    let tmp = t.heap.(i) in
    t.heap.(i) <- t.heap.(j);
    t.heap.(j) <- tmp

  let cmp t i j =
    Ord.compare
      t.heap.(i).elt
      t.heap.(j).elt

  let down i = 2 * i + 1, 2 * i + 2
  let up i = (i - 1) / 2

  let rec cascade_down t i =
    let left, right = down i in
    let go j =
      swap t i j;
      cascade_down t j
    in
    if left < t.len then
      if cmp t i left > 0 then
        if right < t.len && cmp t left right > 0
        then go right
        else go left
      else if right < t.len && cmp t i right > 0 then
        go right;
    update t i

  let rec cascade_up t i =
    let j = up i in
    if i > 0 && cmp t i j < 0 then (
      swap t i j;
      cascade_up t j);
    update t i

  let insert' t x =
    (* Grow by doubling array size. *)
    if t.len = length t.heap then (
      let new_t = make (2 * t.len + 1) x in
      blit t.heap 0 new_t 0 t.len;
      t.heap <- new_t
    ) else
      t.heap.(t.len) <- x;
    cascade_up t t.len;
    t.len <- t.len + 1;
    x

  let delete' t i =
    let x = t.heap.(i) in
    t.len <- t.len - 1;
    if t.len > 0 then (
      t.heap.(i) <- t.heap.(t.len);
      cascade_down t i;
      cascade_up t i);
    (* Shrinking is especially critical for this OCaml implementation
      to avoid memory leaks due to garbage references in the dead part of the
      array. *)
    if t.len < length t.heap / 4 then
      t.heap <- sub t.heap 0 (length t.heap / 2);
    x.heap_ref <- -1;
    x.elt

  (* Turn an array of items into a heap. *)
  (* The update functions are set to nops. *)
  let heapify h =
    let t = { heap = h; len = length h } in
    for i = t.len - 1 downto 0 do
      cascade_down t i
    done;
    t

  let is_empty t = t.len = 0

  let insert t x = insert' t { elt = x ; heap_ref = 0 }

  let delete t r =
    if r.heap_ref < 0
    then invalid_arg "Invalid heap reference."
    else delete' t r.heap_ref

  let delete_min t = delete' t 0

  let peek t = t.heap.(0).elt

  let elt _ r = r.elt

  exception Break

  let check t =
    try
      let check i j =
        if cmp t i j <= 0 then
          raise Break
      in
      if length t.heap < t.len then raise Break;
      for i = 1 to t.len - 1 do
        check i (up i)
      done;
      true
    with Break -> false
end

