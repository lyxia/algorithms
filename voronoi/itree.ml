type 'a t =
  | Node of 'a * 'a t * 'a t
  | Leaf

type 'a ctx0 =
  | Left of 'a * 'a t
  | Right of 'a * 'a t

type 'a ctx = 'a ctx0 list

type 'a loc = 'a t * 'a ctx

let empty = Leaf
let singleton x = Node (x, Leaf, Leaf)
let cons x l r = Node (x, l, r)

(** Context of the leaf defined by [f].
  [f] is a decision function such that
  [f x = false] goes left,
  [f x = true] goes right. *)
let rec find_ctx' f ctx = function
  | Leaf -> ctx
  | Node (x, l, r) ->
      match f x with
      | true -> find_ctx' f (Right (x, l) :: ctx) r
      | false -> find_ctx' f (Left (x, r) :: ctx) l

let find_ctx f = find_ctx' f []

(** Obtain the leaf locator for the same position
  (only the context is returned) *)
let to_ctx (t, ctx) = match ctx with
  | Left (_, _) :: _ -> find_ctx' (fun _ -> true) ctx t
  | Right (_, _) :: _ -> find_ctx' (fun _ -> false) ctx t
  | [] -> invalid_arg "Don't know which way to split."

(** Leaf locator. *)
let to_leaf_loc ctx = (Leaf, ctx)

let rec right_loc t = function
  | Left (x, r) :: ctx -> right_loc (cons x t r) ctx
  | ctx -> t, ctx

let rec left_loc t = function
  | Right (x, l) :: ctx -> left_loc (cons x l t) ctx
  | ctx -> t, ctx

(** Node locator. *)
let to_node_loc ctx = match ctx with
  | Left (_, _) :: _ -> right_loc Leaf ctx
  | Right (_, _) :: _ -> left_loc Leaf ctx
  | [] -> Leaf, []

(** Insert an element and move to the right of it. *)
let insert_ctx x ctx = singleton x, ctx

let insert x loc = insert_ctx x @@ to_ctx loc

(** Replace the element which is above and
  to the right of the position. *)
let rec replace' x t ctx = match left_loc t ctx with
  | l, Left (y, r) :: ctx -> y, (l, Left (x, r) :: ctx)
  | _ -> invalid_arg "No right bound."

let take_ctx = function
  | Left (y, r) :: ctx -> y, (r, ctx)
  (* The deleted element is an internal node,
    replace it by the leaf on its left and move to the right of it. *)
  | Right (x, l) :: ctx -> replace' x l ctx
  | _ -> invalid_arg "No node to delete."

(** Delete the element to the right. *)
let take loc = loc |> to_ctx |> take_ctx

(** Replace the element to the right and move to its right. *)
let exchange x = function
  | l, Left (y, r) :: ctx -> y, (l, Right (x, r) :: ctx)
  | loc -> replace' x Leaf (to_ctx loc)

let exchange_ctx x ctx = ctx |> to_leaf_loc |> exchange x

(** Silenced output. *)
let delete loc = loc |> take |> snd

let replace x loc = loc |> exchange x |> snd

let rec move_left' ctx = match right_loc Leaf ctx with
  | t, Right (x, l) :: ctx -> l, Left (x, t) :: ctx
  | _ -> invalid_arg "No left bound."

(** Move to the left. *)
let move_left = function
  | r, Right (x, l) :: ctx -> l, Left (x, r) :: ctx
  | Leaf, ctx -> move_left' ctx
  | loc ->
      match to_ctx loc with
      | Right (x, l) :: ctx -> l, Left (x, Leaf) :: ctx
      | _ -> invalid_arg "Cannot move left."

(** Reform tree. *)
let zip (t, ctx) =
  let zip f (t, ctx) = match ctx with
    | [] -> t
    | _ -> f t ctx
  in
  let rec z_left t ctx = zip z_right (left_loc t ctx)
  and z_right t ctx = zip z_left (right_loc t ctx)
  in
  z_left t ctx

let zip_ctx ctx = zip (Leaf, ctx)

let rec fold f acc = function
  | Leaf -> acc
  | Node (x, l, r) -> fold f (f (fold f acc l) x) r

let to_list t = fold (fun xs x -> x :: xs) [] t

