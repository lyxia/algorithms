type 'a t =
  | Node of 'a * 'a t * 'a t
  | Leaf

type 'a ctx0 =
  | Left of 'a * 'a t
  | Right of 'a * 'a t

type 'a ctx = 'a ctx0 list

(** Locators represent a position in the tree between two  
  consecutive nodes (in an in-order traversal).

  [l, Left (x, _)] represents the position just before x
  and after the rightmost node in [l].

  [r, Right (x, _)] represents the position just after x
  and before the leftmost node in [r].

  Generally, one position can be represented by two
  locators, one of each type,
  with one of [l] or [r] being a leaf ("leaf locator")
  and the other being a node ("node locator"). *)
type 'a loc = 'a t * 'a ctx

val empty : 'a t
val singleton : 'a -> 'a t
val cons : 'a -> 'a t -> 'a t -> 'a t

val find_ctx : ('a -> bool) -> 'a t -> 'a ctx

val to_ctx : 'a loc -> 'a ctx
val to_leaf_loc : 'a ctx -> 'a loc
val to_node_loc : 'a ctx -> 'a loc

val insert_ctx : 'a -> 'a ctx -> 'a loc
val insert : 'a -> 'a loc -> 'a loc

val take_ctx : 'a ctx -> 'a * 'a loc
val take : 'a loc -> 'a * 'a loc

val exchange : 'a -> 'a loc -> 'a * 'a loc
val exchange_ctx : 'a -> 'a ctx -> 'a * 'a loc

val delete : 'a loc -> 'a loc
val replace : 'a -> 'a loc -> 'a loc

val move_left : 'a loc -> 'a loc

val zip : 'a loc -> 'a t

val zip_ctx : 'a ctx -> 'a t

val fold : ('b -> 'a -> 'b) -> 'b -> 'a t -> 'b

val to_list : 'a t -> 'a list

