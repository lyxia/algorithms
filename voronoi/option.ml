let map f = function
  | None -> None
  | Some x -> Some (f x)

let ap f x = ignore (map f x)

let unwrap = function
  | None -> invalid_arg "Unexpected None."
  | Some x -> x

let (||) = function
  | None -> fun b -> b
  | Some _ as a -> fun _ -> a

