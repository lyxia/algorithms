open Option

type 'a node = {
  mutable elt : 'a;
  mutable up : 'a node option;
  mutable left : 'a link;
  mutable right : 'a link;
}
and 'a link = Down of 'a node | Up of 'a node | Empty

type 'a tree = 'a node ref (* Always at least one node *)

type 'a segment = 'a node option * 'a node option

let elt n = n.elt
let up n = n.up
let left n = n.left
let right n = n.right

let rec set_rightmost m r_ =
  match m.right with
  | Down m -> set_rightmost m r_
  | _ -> m.right <- r_

let rec set_leftmost m l_ =
  match m.left with
  | Down m -> set_leftmost m l_
  | _ -> m.left <- l_

let set_left n l_ =
  n.left <- l_;
  l_ |> function
    | Down l -> l.up <- Some n; set_rightmost l (Up n)
    | _ -> ()

let set_right n r_ =
  n.right <- r_;
  r_ |> function
    | Down r -> r.up <- Some n; set_leftmost r (Up n)
    | _ -> ()

let from_list l =
  let rec split_at n = function
    | x :: xs when n = 0 -> [], x, xs
    | x :: xs ->
        let a, b, c = split_at (n-1) xs in
        x :: a, b, c
    | [] -> assert false
  in
  let rec from_list = function
    | [] -> Empty
    | l ->
        let len = List.length l in
        let a, b, c = split_at (len / 2) l in
        let n = {
          elt = b ;
          up = None ;
          left = Empty ;
          right = Empty } in
        set_left n (from_list a);
        set_right n (from_list c);
        Down n
  in
  match from_list l with
  | Empty -> invalid_arg "Empty list."
  | Down n -> ref n
  | Up _ -> assert false

let rec find f n =
  if f n.elt then
    match n.right with
    | Empty -> (Some n, None)
    | Down m -> find f m
    | Up m -> (Some n, Some m)
  else
    match n.left with
    | Empty -> (None, Some n)
    | Down m -> find f m
    | Up m -> (Some m, Some n)

let search f t = find f !t

let insert2 _ (x_, y_) a b =
  let up_of = function
    | None -> Empty
    | Some z -> Up z in
  let up, finalize = match x_, y_ with
    | None, None -> assert false
    | None, Some y ->
        assert (y.left = Empty);
        y_, fun n -> y.left <- Down n
    | Some x, None ->
        assert (x.right = Empty);
        x_, fun n -> x.right <- Down n
    | Some x, Some y ->
        match x.right, y.left with
        | Down _, Up x' ->
            assert (x == x');
            y_, fun n -> y.left <- Down n
        | Up y', Down _ ->
            assert (y == y');
            x_, fun n -> x.right <- Down n
        | _ -> assert false in
  let rec n = {
    elt = a ;
    up = up ; left = up_of x_ ; right = Down m ;
  } and m = {
    elt = b ;
    up = Some n ; left = Up n ; right = up_of y_ ;
  } in
  finalize n;
  n, m

let merge _ x y a =
  let unwrap = function
    | Down n -> n
    | _ -> assert false in
  match x.right, y.left with
  | Up y', Down z ->
      assert (y' == y);
      let up = Option.unwrap x.up in
      if up == y
      then (
        assert (z == x);
        set_left up x.left
      ) else (
        assert (unwrap up.right == x);
        set_right up x.left;
        set_rightmost up (Up y)
      );
      y.elt <- a;
      y
  | Down w, Up x' ->
      assert (x' == x);
      let up = Option.unwrap y.up in
      if up == x
      then (
        assert (w == y);
        set_right up y.right
      ) else (
        assert (unwrap up.left == y);
        set_left up y.right;
        set_leftmost up (Up x)
      );
      x.elt <- a;
      x
  | _ -> assert false

exception Invalid_tree of string

let check_tree t =
  let check b s = if not b then raise (Invalid_tree s) in
  let eq_up m n =
    match m.up with
    | None -> check false "No link"
    | Some n' -> check (n == n') "Non symmetric link"
  in
  let rec check' l n r =
    begin
      match l, n.left with
      | _, Down m ->
          check' l m (Up n);
          eq_up m n
      | Up l, Up l' -> check (l == l') "Left end mismatch (Up)"
      | Empty, Empty -> ()
      | _ -> check false "Left end mismatch"
    end;
    begin
      match n.right, r with
      | Down m, _ ->
          check' (Up n) m r;
          eq_up m n
      | Up r', Up r -> check (r == r') "Right end mismatch (Up)"
      | Empty, Empty -> ()
      | _ -> check false "Right end mismatch"
    end
  in
  check' Empty !t Empty

let print_tree print t =
  let rec print_tree d n =
    begin
      match n.left with
      | Down m -> print_tree (d+1) m
      | _ -> ()
    end;
    print d n.elt;
    begin
      match n.right with
      | Down m -> print_tree (d+1) m
      | _ -> ()
    end
  in
  print_tree 0 !t

(*

let delete t n =
  ( match n.left, n.right with
    | Some _, _ ->
        let n' = unwrap n.prev in
        let up' = unwrap n'.up in
        assert (n'.right = None);
        up'.right <- n'.left;
        n'.left |> ap (fun l' -> l'.up <- n'.up);
        n'.next <- n.next;
        subs t n n'
    | _, Some _ ->
        let n' = unwrap n.next in
        let up' = unwrap n'.up in
        assert (n'.left = None);
        up'.left <- n'.right;
        n'.right |> ap (fun r' -> r'.up <- n'.up);
        n'.prev <- n.prev;
        subs t n n'
    | None, None ->
        n.prev |> ap (fun prev -> prev.next <- n.next);
        n.next |> ap (fun next -> next.prev <- n.prev);
        subs_up t n None );
  n.elt
let create_n x u elt v y =
  let rec n = {
    elt = elt;
    up = None;
    left = None;
    right = None;
    prev ; next }
  and prev = (x, u, Some n)
  and next = (Some n, v, y) in
  n
*)

(*
let left_end n = find (fun _ -> false) n

let insert t n x =
  let _, u, m = n.next in
  let n' = create_n (Some n) u x u m in
  n.next |> ap (fun next -> next.prev <- (Some n', u, n.next));
  n.next <- (Some n, u, n.next);
  ( match n.right with
    | None -> set_right n n'
    | Some r -> set_left (left_end r) n' );
  n'

let go_up n =
  n.up |> map (function
    | { left = Some l } as up when l == n -> false, up
    | { right = Some r } as up when r == n -> true, up
    | _ -> assert false)

*)
(*
let subs_up t n m' =
  match go_up n with
  | Some (true, up) -> up.left <- m'
  | Some (false, up) -> up.right <- m'
  | None -> t := m'

(** This substitution preserves the consistency of
  references. *)
let subs t n n' =
  let m' = Some n' in
  n'.up <- n.up;
  subs_up t n m';
  n'.left <- n.left;
  n.left |> ap (fun left -> left.up <- m');
  n'.right <- n'.right;
  n.right |> ap (fun right -> right.up <- m')

let replace x n =
  let y = n.elt in
  n.elt <- x;
  y

let fold f acc t =
  let rec fold acc n =
    let acc = f acc n.elt in
    match n.next with
    | None -> acc
    | Some n -> fold acc n
  in
  match !t with
  | None -> acc
  | Some n -> n |> find (fun _ -> false) |> snd |> fold acc
*)
