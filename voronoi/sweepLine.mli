type 'a tree
type 'a node

type 'a segment = 'a node option * 'a node option

val elt : 'a node -> 'a

val from_list : 'a list -> 'a tree
val search : ('a -> bool) -> 'a tree -> 'a segment
val insert2 : 'a tree -> 'a segment -> 'a -> 'a -> 'a node * 'a node
val merge : 'a tree -> 'a node -> 'a node -> 'a -> 'a node

val check_tree : 'a tree -> unit
val print_tree : (int -> 'a -> unit) -> 'a tree -> unit

