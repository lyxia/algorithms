let ranged range = Random.float (snd range -. fst range) +. fst range

let create_test name f () =
  let b = Buffer.create 64 in
  try f b (function true -> () | false -> failwith "Test failed.")
  with
    e ->
      Printf.eprintf "Test: %s\n" name;
      Buffer.output_buffer stderr b;
      raise e

let rec rand_int_list max_val = function
  | 0 -> []
  | n -> Random.int max_val :: rand_int_list max_val (n-1)

let rec rand_float_list max_val = function
  | 0 -> []
  | n -> Random.float max_val :: rand_float_list max_val (n-1)

module Boundary = struct
  open Types

  let rec random_boundary ?(range=(0.,10.)) () =
    let float () = ranged range in
    let u, v =
      let uv = List.sort compare [(float (), float ()) ; (float (), float ())] in
      match uv with | [v; u] -> u, v | _ -> assert false in
    if fst u = fst v
    then mk_boundary u v Zero ((snd u +. snd v) /. 2.)
    else mk_boundary u v (if Random.bool () then Plus else Minus) (snd u)

  (* Check the equidistance property of boundary_(x|y). *)
  let test_boundary_xy ?(threshold=0.001) =
    create_test "boundary_xy" (fun b check ->
      let printf fmt = Printf.bprintf b fmt in
      let { u ; v } as boundary = random_boundary () in
      let px = max (fst boundary.u) (fst boundary.v) +. Random.float 10. in
      printf "u = (%f, %f)\n" (fst u) (snd u);
      printf "v = (%f, %f)\n" (fst v) (snd v);
      printf "px = %f\n" px;
      let s = (boundary_x boundary px, boundary_y boundary px) in
      printf "s = (%f, %f)\n" (fst s) (snd s);
      let a = distance2 u s and b = distance2 v s and c = distance2 (px, snd s) s in
      printf "us = %f\nvs = %f\nps = %f\n" a b c;
      let (=.) = float_eq threshold in
      check (a =. b && b =. c))

  (* Compare to a (presumably) slower implementation using boundary_y. *)
  let test_site_boundary_test ?(iterations=3) () =
    create_test "site_boundary_test" (fun b check ->
      let printf fmt = Printf.bprintf b fmt in
      let { u ; v } as boundary = random_boundary () in
      let ps = ref [fst u, 0.] in
      printf "u = (%f, %f)\n" (fst u) (snd u);
      printf "v = (%f, %f)\n" (fst v) (snd v);
      for i = 1 to iterations do
        let p = Random.(float 1. +. fst (List.hd !ps), float 10.) in
        printf "p_%d = (%f, %f)\n" i (fst p) (snd p);
        ps := p :: !ps;
        let test = site_boundary_test boundary p in
        let y = boundary_y boundary (fst p) in
        printf "\ny = %f\n" y;
        printf "%s\n" (string_of_dir test);
        check (bool_of_dir test = (y <= snd p))
      done)
end

module Heap = struct
  type z = { value : int ; mutable flag : bool }
  module TestHeap = Heap.Make(struct
    type elt = z
    let compare x y = compare x.value y.value
  end)

  let rec is_sorted = function
    | y :: (y' :: _ as ys) -> y <= y' && is_sorted ys
    | _ -> true

  let test ?(max_length=10) ?(max_val=100) =
    create_test "heap" (fun b check ->
      let printf fmt = Printf.bprintf b fmt in
      let print_list xs =
        match xs with
        | [] -> printf "\n"
        | x :: xs ->
            printf "%d" x;
            List.iter (printf ", %d") xs;
            printf "\n"
      in
      let n = Random.int max_length in
      let xs = rand_int_list max_val n in
      print_list xs;
      let xs = List.map (fun x -> { value = x ; flag = false }) xs in
      let h = TestHeap.create () in
      let m = ref 0 in
      let rs = List.map (fun x -> TestHeap.insert h x) xs in
      if n > 0 then (
        let r = List.nth rs (Random.int n) in
        let y = TestHeap.delete h r in
        incr m;
        printf "%d\n" y.value;
        y.flag <- true
      );
      let rec extract () =
        if TestHeap.is_empty h then [] else
          let y = TestHeap.delete_min h in
          incr m;
          y.flag <- true;
          y.value :: extract ()
      in
      let ys = extract () in
      print_list ys;
      check (!m = n && is_sorted ys && List.for_all (fun x -> x.flag) xs))
end

module SweepLine = struct
  let test ?(iterations=100) ?(init_length=10) =
    create_test "sweepLine" (fun b check ->
      let iterations = Random.int iterations in
      let printf fmt = Printf.bprintf b fmt in
      let n = 1 + Random.int init_length in
      let max_val = ref 1. in
      let print_list xs =
        match xs with
        | [] -> printf "\n"
        | x :: xs ->
            printf "%f" x;
            List.iter (printf ", %f") xs;
            printf "\n"
      in
      let xs = List.sort compare (rand_float_list !max_val n) in
      print_list xs;
      let t = SweepLine.from_list xs in
      SweepLine.check_tree t;
      for i = 1 to iterations do
        let x = Random.float !max_val in
        let seg = SweepLine.search (fun y -> y <= x) t in
        max_val := !max_val *. 1.01;
        match seg with
        | Some u, Some v when Random.bool () ->
            printf "Merge: %f %f -> %f\n" (SweepLine.elt u) (SweepLine.elt v) x;
            ignore (SweepLine.merge t u v x)
        | _, Some v ->
            let y = (x +. SweepLine.elt v) /. 2. in
            printf "Insert: %f (%f %f) %f\n"
              (match fst seg with None -> neg_infinity | Some u -> (SweepLine.elt u))
              x y (SweepLine.elt v);
            ignore (SweepLine.insert2 t seg x y)
        | Some u, None ->
            let y = x +. 1. in
            printf "Insert: %f (%f %f) %f\n"
              (SweepLine.elt u) x y infinity;
            ignore (SweepLine.insert2 t seg x y)
        | _ -> assert false
      done;
      SweepLine.check_tree t
    )
end
