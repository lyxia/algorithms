(* Basic functions *)
let square x = x *. x
let float_eq threshold a b =
  abs_float (a -. b) /. (abs_float a +. abs_float b) < threshold

(* (x,y)-coordinates.
 * Points are ordered lexicographically (as is done by the default OCaml
 * ordering):
 * (ux, uy) > (vx, vy) iff ux > vx || (ux = vx && uy > vy).
 *
 * Visual convention: The x-axis is horizontal and increases to the right,
 * while the y-axis is vertical and increases upwards.
 *
 * >  y
 * >  ^
 * >  |
 * >  +--> x
 *
 * We implement the line-sweep left-to-right.
 *)
type point = float * float

let compare_points = compare
let sort_points l = List.sort compare_points l
let distance2 (ux, uy) (vx, vy) = square (ux -. vx) +. square (uy -. vy)

let print_point_list ps =
  match ps with
  | [] -> Printf.printf "\n"
  | (x, y) :: ps ->
      Printf.printf "(%.3f, %.3f)" x y;
      List.iter (fun (x, y) -> Printf.printf ", (%.3f, %.3f)" x y) ps;
      Printf.printf "\n"

let circumcenter (ax, ay) (bx, by) (cx, cy) =
  let aby = ay -. by in
  let bcy = by -. cy in
  let cay = cy -. ay in
  let d = 2. *. (ax *. bcy +. bx *. cay +. cx *. aby) in
  if d = 0.
  then None
  else
    let a2 = square ax +. square ay in
    let b2 = square bx +. square by in
    let c2 = square cx +. square cy in
    let x = (a2 *. bcy +. b2 *. cay +. c2 *. aby) /. d in
    let y = (a2 *. (cx -. bx) +. b2 *. (ax -. cx) +. c2 *. (bx -. ax)) /. d in
    Some (x, y)

(* Take points with minimal x-coordinate. *)
let split_min l =
  let rec take x = function
    | (x', _) as p :: xs when x = x' ->
        let x0, x1 = take x xs in
        p :: x0, x1
    | x1 -> ([], x1)
  in
  match l with
  | [] -> invalid_arg "split_min: empty list"
  | ((x, _) :: _) as xs -> take x xs

type site = point
type vertex = point

type b_direction = Minus | Zero | Plus
type direction = Down | Up

let bool_of_dir = function
  | Down -> false
  | Up -> true

let dir_of_bool = function
  | false -> Down
  | true -> Up

let string_of_dir = function
  | Down -> "Down"
  | Up -> "Up"

(* A boundary is a subset of the bisector of two sites u and v (u > v).
 *
 * This actually refers to a half-boundary, i.e., a ray which can either be
 * horizontal (iff ux = uy) or extend in one of two directions: up (plus-) or
 * down (minus-boundary).
 *
 * As the sweep line advances to the right, an increasing part of that ray
 * is guaranteed to be a subset of the Voronoi diagram, and the "history" field
 * gives a conservative bound on the y-coordinate of the moving extremity of
 * that confirmed subset. It can be used to speed up comparisons during
 * lookups in the sweep line. *)
type boundary = {
  u : point ;
  v : point ;
  b_dir : b_direction ;
  b_end : point option ref ;
  mutable predicted_intersect_up : intersect' Heap.r' option ;
  mutable predicted_intersect_down : intersect' Heap.r' option ;
  mutable history : float ;
  (* Use previous comparisons to speed up subsequent ones.
    /!\ Stateful! Assumes that candidates are presented in lexicographic
    order. *)
}
and intersect = site * ray * vertex * ray * site
and intersect' = point * intersect
and ray = boundary SweepLine.node

let vertex_of_intersect (_, _, v, _, _) = v

module IHeap = Heap.Make(struct
    type elt = intersect'
    let compare a b = compare (fst a) (fst b)
  end)

let check_boundary { u ; v } = u > v

(* u > v *)
let mk_boundary u v b_dir y0 =
  assert (u > v);
  { u ; v ; b_dir ;
    b_end = ref None ;
    predicted_intersect_up = None ;
    predicted_intersect_down = None ;
    history = y0 }

(* boundary_(x|y): coordinates of the point of a boundary ray which is equidistant
 * to the neighbor sites and to the vertical line x=px.
 * These two functions give reference implementation used to test
 * site_boundary_test. *)
let boundary_x { u = (ux, uy) ; v = (vx, vy) ; b_dir } px =
  let uvx = ux -. vx in
  let uvy = uy -. vy in
  let pux = px -. ux in
  let pvx = px -. vx in
  let slope = uvy /. uvx in
  let slope2 = square slope in
  let r = (ux +. vx -. (pux +. pvx) *. slope2) /. 2. in
  let d = slope *. sqrt (pux *. pvx *. (1. +. slope2)) in
  match b_dir with
  | Minus -> r +. d
  | Plus -> r -. d
  | Zero -> nan

let boundary_y { u = (ux, uy) ; v = (vx, vy) ; b_dir } px =
  let uvx = ux -. vx in
  let uvy = uy -. vy in
  let pux = px -. ux in
  let pvx = px -. vx in
  let slope2 = square (uvy /. uvx) in
  let r = (uy *. pvx -. vy *. pux) /. uvx in
  let d = sqrt (pux *. pvx *. (1. +. slope2)) in
  match b_dir with
  | Plus -> r +. d
  | Minus -> r -. d
  | Zero -> nan

(* Determine which side of a transformed C_uv boundary a point p=(px,py)
 * belongs to. *)
(* The y-coordinate of the intersection of the transformed C_uv boundary
 * with the sweep line at px is a solution of a certain quadratic equation:
 * Ay^2 + By + C = 0. We compare this to py. *)
(* If p is on the boundary, we return [Up] by default. *)
let site_boundary_test ?(history=true) boundary (px, py) =
  let { u = (ux, uy); v = (vx, vy); b_dir; history = h } = boundary in
  let delayed () =
    let uvx = ux -. vx in
    let pvx = px -. vx in
    let pux = px -. ux in
    let uvy = uy -. vy in
    let rhs = py *. uvx +. vy *. pux -. uy *. pvx in
    let delta = pvx *. pux *. (square uvy +. square uvx) in
    rhs, delta in
  match b_dir with
  | Zero -> if h > py then Down else Up
  | Plus ->
      if history && h > py
      then Down
      else
        let rhs, delta = delayed () in
        if rhs < 0. || delta > rhs *. rhs
        then (boundary.history <- py; Down)
        else Up
  | Minus ->
      if history && h <= py
      then Up
      else
        let rhs, delta = delayed () in
        if rhs < 0. && delta < rhs *. rhs
        then Down
        else (boundary.history <- py; Up)

(* Requires px > qx *)
let new_boundary (p : site) (q : site) =
  assert (fst p > fst q);
  let b b_dir = mk_boundary p q b_dir (snd p) in
  b Minus, b Plus

let new_boundary' q s (_, py) =
  let q, s = if q >= s then q, s else s, q in
  let b_dir = if fst q = fst s then Zero else if py < snd q then Minus else Plus in
  mk_boundary q s b_dir py

let rec new_boundaries = function
  | q :: (p :: _) as ps ->
      let b = mk_boundary p q Zero ((snd p +. snd q) /. 2.) in
      b :: new_boundaries ps
  | _ -> []


(* Sweep line *)

type region = boundary SweepLine.segment

type sweep_line = boundary SweepLine.tree

let lookup_region (p : site) (line : sweep_line) =
  SweepLine.search (fun bdry -> site_boundary_test bdry p |> bool_of_dir) line

let intersect h p ray_pq i ray_qs s =
  let r = IHeap.insert h
    ((fst i +. sqrt (distance2 p i), snd i), (p, ray_pq, i, ray_qs, s)) in
  (SweepLine.elt ray_pq).predicted_intersect_up <- Some r;
  (SweepLine.elt ray_qs).predicted_intersect_down <- Some r

let deintersect q b c =
  match b.predicted_intersect_up, c.predicted_intersect_down with
  | None, None -> ()
  | Some r, Some r' ->
      assert (r == r');
      ignore (IHeap.delete q r);
      b.predicted_intersect_up <- None;
      c.predicted_intersect_down <- None
  | _ -> assert false

let sites_of ray =
  let b = SweepLine.elt ray in
  match b.b_dir with
  | Plus -> b.u, b.v
  | Minus | Zero -> b.v, b.u


(* Event queue *)

type event
  = Site of site
  | Intersect of intersect

type queue = {
  mutable sites : site list ;
  intersects : IHeap.t ;
}

let new_queue (sites : point list) = {
  sites ;
  intersects = IHeap.create () ;
}

let pop queue = match queue.sites, IHeap.is_empty queue.intersects with
  | [], true -> None
  | s :: ss, true ->
      queue.sites <- ss; Some (Site s)
  | s :: ss, false when s < fst (IHeap.peek queue.intersects) ->
      queue.sites <- ss; Some (Site s)
  | _, false -> Some (Intersect (snd (IHeap.delete_min queue.intersects)))

let rec queue_loop (q : queue) (f : event -> unit) =
  match pop q with
  | None -> ()
  | Some e -> f e; queue_loop q f


(* (u, v, p, q) describes the segment of the bisector of p and q from u to v,
 * which can be open-ended (u or v = None) in either direction, i.e., a ray, or
 * even both, i.e., a line.
 *
 * The direction of the vector (uv) should be the image of the direction of
 * the vector (pq) by a clockwise rotation through a right angle.
 *
 * The refs will be filled progressively as intersection events are encountered.
 * *)
type tmp_edge = point option ref * point option ref * point * point

let finalize_edge (u, v, p, q) =
  match !u, !v with
  | None, None -> Diagram.Line (p, q)
  | Some u, Some v -> Diagram.Edge (u, v)
  | Some u, None ->
      let dir = snd q -. snd p, fst p -. fst q in
      Diagram.Ray (u, dir)
  | None, Some v ->
      let dir = snd p -. snd q, fst q -. fst p in
      Diagram.Ray (v, dir)

