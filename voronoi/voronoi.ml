open Types

let debug = false

(* The main algorithm. *)

(* length sites >= 2 *)
let voronoi' (sites : point list) =
  let open Option in
  let edges = ref ([] : tmp_edge list) in
  let vertices = ref [] in
  let write_edge u v p q = edges := (u, v, p, q) :: !edges in
  let write_vertex vertex = vertices := vertex :: !vertices in
  let finalize_edges () = List.map finalize_edge !edges in
  let line, queue =
    match sites |> sort_points |> split_min with
    | ([] | [_]), [] -> assert false
    | [q], p :: ps ->
        let line =
          let bound_qp_m, bound_pq_p = new_boundary p q in
          write_edge bound_qp_m.b_end bound_pq_p.b_end p q;
          SweepLine.from_list [bound_qp_m ; bound_pq_p]
        and queue = new_queue ps in
        line, queue
    | qs, ps ->
        let line =
          let bs = new_boundaries qs in
          let inf = ref None in
          List.iter (fun b -> write_edge b.b_end inf b.u b.v) bs;
          SweepLine.from_list bs
        and queue = new_queue ps in
        line, queue
  in
  let deintersect = deintersect queue.intersects in
  let intersect f p ray_pq q ray_qr r =
    circumcenter p q r |> ap (fun s ->
      if f s then intersect queue.intersects p ray_pq s ray_qr r)
  in
  let if_above p v = snd p < snd v in
  let if_below p v = snd p > snd v in
  let anyway _ = true in
  queue_loop queue (function
    | Site p ->
        if debug then Printf.eprintf "Site (%f, %f)\n" (fst p) (snd p);
        let ray_rq_, ray_qs_ as rays = lookup_region p line in
        let r_q_ = map sites_of ray_rq_ in
        let q_s_ = map sites_of ray_qs_ in
        let r_ = map fst r_q_ in
        let s_ = map snd q_s_ in
        let q = unwrap Option.(map snd r_q_ || map fst q_s_) in
        let bound_qp_m, bound_pq_p = new_boundary p q in
        let ray_qp_m, ray_pq_p = SweepLine.insert2 line rays bound_qp_m bound_pq_p in
        write_edge bound_qp_m.b_end bound_pq_p.b_end p q;
        ray_rq_ |> ap (fun ray_rq -> ray_qs_ |> ap (fun ray_qs ->
          deintersect (SweepLine.elt ray_rq) (SweepLine.elt ray_qs)));
        r_ |> ap (fun r -> ray_rq_ |> ap (fun ray_rq ->
          intersect (if_below p) r ray_rq q ray_qp_m p));
        s_ |> ap (fun s -> ray_qs_ |> ap (fun ray_qs ->
          intersect (if_above p) p ray_pq_p q ray_qs s))
    | Intersect (q, ray_qr, p, ray_rs, s) ->
        if debug then Printf.eprintf "Intr (%f, %f)\n" (fst p) (snd p);
        write_vertex p;
        let p_ = Some p in
        let bound_qr = SweepLine.elt ray_qr in
        let bound_rs = SweepLine.elt ray_rs in
        bound_qr.b_end := p_;
        bound_rs.b_end := p_;
        let bound_qs = new_boundary' q s p in
        let on_edge, low, high =
          match bound_qs.b_dir with
          | Zero -> anyway, bound_qr.b_end, bound_qs.b_end
          | Plus -> if_above p, bound_qr.b_end, bound_qs.b_end
          | Minus -> if_below p, bound_qs.b_end, bound_qr.b_end
        in
        write_edge low high bound_qs.u bound_qs.v;
        let ray_qs = SweepLine.merge line ray_qr ray_rs bound_qs in
        bound_qr.predicted_intersect_down |> ap (fun r ->
          let u, ray_uq, _, _, _ = IHeap.elt queue.intersects r |> snd in
          deintersect (SweepLine.elt ray_uq) bound_qr;
          intersect on_edge u ray_uq q ray_qs s);
        bound_rs.predicted_intersect_up |> ap (fun r ->
          let _, _, _, ray_sv, v = IHeap.elt queue.intersects r |> snd in
          deintersect bound_rs (SweepLine.elt ray_sv);
          intersect on_edge q ray_qs s ray_sv v)
    );
  { Diagram.vertices = !vertices ; Diagram.edges = finalize_edges () }

let voronoi = function
  | [] | [_] -> Diagram.{ vertices = [] ; edges = [] }
  | sites -> voronoi' sites

